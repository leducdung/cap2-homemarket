import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link, NavLink
} from "react-router-dom";
import Login from '../pageUser/Login';

export default function Menu() {
  return (
    <div className="sticky-top" style={{ padding: '0px', margin: '0px',zIndex:'101' }}>
      <section className="menu container-fluid ">
        <nav style={{ backgroundColor: 'white' ,boxShadow:'0 1px 8px #555555' }} id="menu" className="navbar navbar-expand-lg navbar-dark text-center pt-4 pb-4">
          <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Home">HOME MARKET</NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span style={{ color: 'white' }} className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse pr-3" id="navbarText">
            <ul className="navbar-nav mr-auto justify-content-center">
              <li className="nav-item active">
                <NavLink className="nav-link js-scroll-trigger MenuRow1" to="/Home">TRANG CHỦ</NavLink>

              </li>
              <li className="nav-item">
                <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Introduce">GIỚI THIỆU</NavLink>
              </li>
              <li className="nav-item dropdown">
                <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Industry">NGÀNH HÀNG</NavLink>
              </li>
              <li className="nav-item ml-2 mr-2">
                <NavLink className="nav-link js-scroll-trigger MenuRow" to="/KhuyenMai">KHUYẾN MÃI</NavLink>
              </li>
            </ul>
            <span className="navbar-text pr-3">
            <input type="search"
                  className="form-control search " name="" id="" aria-describedby="helpId" placeholder="Search"/>
            </span>
            <span className="navbar-text pr-3">

              <NavLink className="nav-link cartMenu" to="/Giohang"><i className="fa fa-cart-arrow-down" aria-hidden="true"></i></NavLink>
            </span>
            <span className="navbar-text pr-3">
              <button style={{ border: 'none' }} className="btnlogin p-2" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Đăng
                Nhập</button>
              <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog text-center">
                  <div style={{ width: '890px' }} className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title" id="exampleModalLabel">Sign in</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div style={{ color: '#747487' }} className="container form">
                      <div className="row">
                        <div style={{ color: '#fff' }} className="col-md-6 imglogin pt-5">
                          <div className="text-center pt-5">
                            <h1 className="login-title">ĐĂNG NHẬP</h1>
                            <h4 className="login-title mb-5">Chào mừng trở lại!</h4>
                            <h6 className="pt-5">Bạn mới sử dụng Home Martket? <NavLink className="nav-link js-scroll-trigger" to="/Register">Đăng kí miễn phí</NavLink>
                            </h6>
                          </div>
                        </div>
                        <div className="col-md-6 pt-5 pr-5 pl-5">
                          <Login></Login>
                          <form className="signwith pb-4">
                            <div className="row">
                              <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                              <div className="col-md-4 text-center"><small>Hoặc đăng nhập bằng</small></div>
                              <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                            </div>
                            <div className="d-grid gap-2 d-md-block text-center">
                              <a className="btn fb" href="#" role="button">
                                <div className="icon_fb icon2">
                                  <i className="fa fa-facebook" aria-hidden="true" />
                                </div> <br />
                                <h6>Facebook</h6>
                              </a>
                              <a className="btn gm" href="#" role="button">
                                <div className="icongm icon2">
                                  <i className="fa fa-google" aria-hidden="true" />
                                </div><br />
                                <h6>Gmail</h6>
                              </a>
                            </div>
                            <small className="pdt-md pt-4" id="agree-terms">
                              <font style={{ verticalAlign: 'inherit' }}>
                                <font style={{ verticalAlign: 'inherit' }}>
                                  Bằng cách đăng nhập, tôi đồng ý với </font>
                              </font><a className="rule" target="_blank" href="/privacy">
                                <font style={{ verticalAlign: 'inherit' }}>
                                  <font style={{ verticalAlign: 'inherit' }}>Chính sách Bảo mật</font>
                                </font>
                              </a>
                              <font style={{ verticalAlign: 'inherit' }}>
                                <font style={{ verticalAlign: 'inherit' }}> và </font>
                              </font><a className="rule" target="_blank" href="/privacy">
                                <font style={{ verticalAlign: 'inherit' }}>
                                  <font style={{ verticalAlign: 'inherit' }}>Điều Khoản Dịch Vụ</font>
                                </font>
                              </a>
                              <font style={{ verticalAlign: 'inherit' }}>
                                <font style={{ verticalAlign: 'inherit' }}> .</font>
                              </font>
                            </small>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </span>
            <span className="navbar-text">
              {/* <div className="dropdown">
                <button className="" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                      <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30} fill="currentColor" className="bi bi-cart-plus" viewBox="0 0 16 16">
                <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
                <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h6a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
              </svg>

                    </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                  <button className="dropdown-item" href="#">Action</button>
                  <button className="dropdown-item disabled" href="#">Disabled action</button>
                </div>
              </div> */}

            </span>
          </div>
        </nav>

      </section>
    </div >




  )

}
