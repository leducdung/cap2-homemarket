import React from 'react';
import PropTypes from 'prop-types';

CardDemo.propTypes = {
    
};

function CardDemo(props) {
    return (
        <div>
            <div style={{backgroundColor:'white', borderTop:'solid 5px orange'}} className="row mt-3">
            <div className="col-md-3 col-sm-6 mt-4 mb-4">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-left ml-2">
                                            <small className="sieuthi mb-3">Lotel mart</small>
                                            <p className="card-title tensp">Dâu Tây</p>
                                            <small className="trongluong">300g</small>
                                            <p className="gia mt-4">159,000đ</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
            <div className="col-md-3 col-sm-6 mt-4 mb-4">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-left ml-2">
                                            <small className="sieuthi mb-3">Lotel mart</small>
                                            <p className="card-title tensp">Dâu Tây</p>
                                            <small className="trongluong">300g</small>
                                            <p className="gia mt-4">159,000đ</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
            <div className="col-md-3 col-sm-6 mt-4 mb-4">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-left ml-2">
                                            <small className="sieuthi mb-3">Lotel mart</small>
                                            <p className="card-title tensp">Dâu Tây</p>
                                            <small className="trongluong">300g</small>
                                            <p className="gia mt-4">159,000đ</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
            <div className="col-md-3 col-sm-6 mt-4 mb-4">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-left ml-2">
                                            <small className="sieuthi mb-3">Lotel mart</small>
                                            <p className="card-title tensp">Dâu Tây</p>
                                            <small className="trongluong">300g</small>
                                            <p className="gia mt-4">159,000đ</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                                        
            </div>
        </div>
    );
}

export default CardDemo;