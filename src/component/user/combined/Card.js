import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link, NavLink
  } from "react-router-dom";
export default  function Card(props) {
    const { oneProduct } = props
     return  (       
            <div className="col-md-3 mb-4">
                <div className="card">
                    <div className="khoi">
                        <div className="layer1">
                            <img className="card-img-top" src={oneProduct.photos} alt="" />
                        </div>
                        <div style={{ width: '100%' }} className="layer2">
                            <div className="icon-card">
                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                            </div>
                            <div className="icon-card ml-2">
                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                            </div>
                        </div>
                        <div className="card-body text-left ml-2">
                            <small className="sieuthi mb-3">Lotel mart</small> <br></br>
                            <NavLink  className="card-title tensp" to="/Product">{oneProduct.name}</NavLink><br></br>
                            <small className="trongluong">{oneProduct.nextWeight}</small>
                            <p className="gia mt-4">{oneProduct.price}</p>
                        </div>
                    </div>
                </div>
            </div>
 
    )
}

