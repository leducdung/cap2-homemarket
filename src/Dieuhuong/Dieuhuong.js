import React, { useEffect, useState } from 'react';
import {
    // BrowserRouter as Router,
    Route,
  }
from "react-router-dom";
// import App from '../App';
import Home from "../pageUser/Home";
import Industry from '../pageUser/Industry';
import InfomationStore from '../pageUser/InfomationStore';
import Introduce from '../pageUser/Introduce';
import KhuyenMai from '../pageUser/KhuyenMai';
import Product from '../pageUser/Product';
import Register from '../pageUser/Register';
import Giohang from '../pageUser/Giohang';
import InfomationUser from '../pageUser/InfomationUser';
import Beat from '../pageUser/Beat';
import MilkEgg from '../pageUser/MilkEgg';
import Poultry from '../pageUser/Poultry';
import Frozenfood from '../pageUser/Frozenfood';
import Seafood from '../pageUser/Seafood';
import Bread from '../pageUser/Bread';
import Vegartable from '../pageUser/Vegartable';
import Fruit from '../pageUser/Fruit';
import ResetPassword from '../pageUser/ResetPassword';
import ManyStore from '../pageUser/ManyStore';
import AddCard from '../pageUser/AddCard';
import RegisterStore from '../pageStore/RegisterStore';
function Dieuhuong( props) {

  return (
    <div>
      <Route exact path="/Home"> <Home  ></Home> </Route>
      <Route path="/Introduce">  <Introduce></Introduce> </Route>
      <Route path="/Industry">  <Industry></Industry> </Route>
      <Route path="Product"> <Product /> </Route>
      <Route path="/KhuyenMai">  <KhuyenMai></KhuyenMai> </Route>
      <Route path="/Register">  <Register></Register> </Route>
      <Route path="/InformationStore">  <InfomationStore></InfomationStore> </Route>
      <Route path="/Giohang">  <Giohang></Giohang> </Route>
      <Route path="/InformationUser">  <InfomationUser></InfomationUser> </Route>
      <Route path="/Beat">  <Beat></Beat> </Route>
      <Route path="/MilkEgg">  <MilkEgg></MilkEgg> </Route>
      <Route path="/Poultry">  <Poultry></Poultry> </Route>
      <Route path="/Frozenfood">  <Frozenfood></Frozenfood> </Route>
      <Route path="/Seafood">  <Seafood></Seafood> </Route>
      <Route path="/Bread">  <Bread></Bread> </Route>
      <Route path="/Vegartable">  <Vegartable></Vegartable> </Route>
      <Route path="/Fruit">  <Fruit></Fruit></Route>
      <Route path="/ResetPassword"> <ResetPassword></ResetPassword> </Route>
      <Route path="/ManyStore"> <ManyStore></ManyStore> </Route>
      <Route path="/AddCard"> <AddCard></AddCard> </Route>
      <Route path="/RegisterStore"> <RegisterStore></RegisterStore> </Route>
    </div>
  );
}

export default Dieuhuong;