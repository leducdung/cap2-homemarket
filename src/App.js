import './App.css';
import React from 'react';
import Dieuhuong from "./Dieuhuong/Dieuhuong";
import {
  BrowserRouter as Router,
} from "react-router-dom";

function App() {

  return (
    <Router>
      <div className="App">
          <Dieuhuong />
      </div>
    </Router>
  );
}

export default App;
