import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
  }
from "react-router-dom";
import App from '../App';
import Home from "../pageUser/Home";
import Industry from '../pageUser/Industry';
import InfomationStore from '../pageUser/InfomationStore';
import Introduce from '../pageUser/Introduce';
import KhuyenMai from '../pageUser/KhuyenMai';
import Product from '../pageUser/Product';
import Register from '../pageUser/Register';
import Giohang from '../pageUser/Giohang';
import InfomationUser from '../pageUser/InfomationUser';
function Dieuhuong() {
  return (
    <div>
      <Route exact path="/Home"> <Home></Home> </Route>
      <Route path="/Introduce">  <Introduce></Introduce> </Route>
      <Route path="/Industry">  <Industry></Industry> </Route>
      <Route path="Product"> <Product /> </Route>
      <Route path="/KhuyenMai">  <KhuyenMai></KhuyenMai> </Route>
      <Route path="/Product">  <Product></Product> </Route>
      <Route path="/Register">  <Register></Register> </Route>
      <Route path="/InformationStore">  <InfomationStore></InfomationStore> </Route>
      <Route path="/Giohang">  <Giohang></Giohang> </Route>
      <Route path="/InformationUser">  <InfomationUser></InfomationUser> </Route>
    </div>
  );
}

export default Dieuhuong;