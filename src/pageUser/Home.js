import Danhmucsp from "../component/Danhmucsp";
import Sphamkhuyenmai from "../component/Sphamkhuyenmai";
import Store from "../component/Store";
import Spbanchay from "../component/user/Spbanchay";
import React, { useEffect, useState } from 'react';
// import Danhmucsp from './component/Danhmucsp';
// import Footer from './component/Footer';
// import Menu from './component/Menu';
// import Spbanchay from './component/user/Spbanchay';
// import Sphamkhuyenmai from './component/Sphamkhuyenmai';
// import Store from './component/Store';

import axios from 'axios';
import Loadpage from "../component/Loadpage";
import Slide from "../component/Slide";

import MenuTow from '../component/Menu';
import Footer from '../component/Footer';
import Menu from "../component/Menu";

function Home() {
  console.log('loadApp')
  const [product, setProduct] = useState();

  const fetchData = async () => {
    console.log('call apis')
      const result = await axios.get('http://homemarket-hm.us-3.evennode.com/products', {
        // headers: {
        //   Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImxlZHVjZHVuc2dmZGQ3IiwicmVzdWx0Ijp7InN0b3JlT3duZXJJRCI6eyJuYW1lIjpudWxsLCJwaG90b3MiOm51bGwsImVtYWlsIjpudWxsLCJkZXNjcmlwdGlvbiI6bnVsbCwicGhvbmVOdW1iZXJzIjpudWxsLCJhZGRyZXNzIjpudWxsLCJzdGF0dXMiOiJOT1NUQVRVUyIsIl9pZCI6IjYwNWFiMWJlNjVhZWI1YzkwZmRjNmRjMCIsImNyZWF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguMTkxWiIsInVwZGF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguMTkxWiIsIl9fdiI6MH0sImVtcGxveWVlSUQiOm51bGwsInByb2ZpbGVQaWN0dXJlIjpudWxsLCJnZW5kZXIiOm51bGwsImZ1bGxOYW1lIjoibGUgZHVuZyAxIiwiZW1haWwiOm51bGwsInBob25lIjpudWxsLCJmYWNlYm9va0lEIjpudWxsLCJnb29nbGVJRCI6bnVsbCwic3RhdHVzIjoiVU5MT0NLIiwicm9sZVBlbmRpbmciOiJCQVNJQyIsInJvbGUiOiJBRE1JTiIsIl9pZCI6IjYwNWFiMWJlNjVhZWI1YzkwZmRjNmRjMSIsInVzZXJOYW1lIjoibGVkdWNkdW5zZ2ZkZDciLCJwYXNzV29yZCI6IiQyYiQxMCRhV2VVTnMzdFlhemtDLmVPSVZQMzhlTXFrL1lOWURRbkxHMlpUSkg4d0Y4b1cwWHFJOW1wTyIsImNyZWF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguNDI2WiIsInVwZGF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguNDI2WiIsIl9fdiI6MH0sImlhdCI6MTYxODc1MTAyMiwiZXhwIjoxNjM0MzAzMDIyfQ.aXTDCarp5r6GZfm6aAQi_LKNqx4gW4o1o--LGYgkObI',
        // },
      })

    return result.data
  }

  useEffect(() => {
    console.log('call useEffect')
    fetchData().then(data => {
      setProduct(data)
    })
  }, [ ])

if(!product || product.lengh === 0 ){
  return <Loadpage></Loadpage>
}

console.log(product)

  return (
    <>
   <Menu></Menu>
    <div className="Home">
        <div className="cha">
        <Slide></Slide>
        <Danhmucsp></Danhmucsp>
        </div>
      <Spbanchay product={product} ></Spbanchay>
      <Sphamkhuyenmai></Sphamkhuyenmai>
      <Store></Store>
    <h1>hoc gitj</h1>
    </div>
    <Footer></Footer>
    </>
  );
}
export default Home;
