import React from 'react';
import useForm from "../component/Userform";
import validate from '../component/Loginformvalidationrule';
function Login() {
    const {
        values,
        errors,
        handleChange,
        handleSubmit,
      } = useForm(login, validate);
      function login() {
        console.log('No errors, submit callback called!');
      }
    return (
        <div className="section is-fullheight">
        <div className="column is-4 is-offset-4">
          <div className="box">
            <form onSubmit={handleSubmit} noValidate>
              <div className="field text-left">
                <label className="label">Email Address</label>
                <div className="control">
                  <input autoComplete="off" className={`input ${errors.email && 'is-danger'} w-100 form-control`} type="email" name="email" onChange={handleChange} value={values.email || ''} required />
                  {errors.email && (
                    <p className="help is-danger">{errors.email}</p>
                  )}
                </div>
              </div>
              <div className="field text-left mt-3">
                <label className="label">Password</label>
                <div className="control">
                  <input className={`input ${errors.password && 'is-danger'} w-100 form-control`} type="password" name="password" onChange={handleChange} value={values.password || ''} required />
                </div>
                {errors.password && (
                  <p className="help is-danger">{errors.password}</p>
                )}
              </div>
              <button type="submit" className="button is-block is-info is-fullwidth btnlogin mt-3 p-2">Login</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default Login;