import React, { useEffect,
  //  useReducer,
   useState } from 'react';
import axios from "axios";
import MenuTow from '../component/MenuTwo';
import Footer from '../component/Footer';
import { API } from '../config/ConfigENV';
function Register() {
        const [register, setRegister] = useState('')
        const [resultRegister, setResultRegister] = useState('1')
        const fetchData = async () => {
            const result = await axios({
                method: 'post',
                url: `${API}/register`,
                data: {
                    ...register,
                },
            })
            await setResultRegister(result)
            await console.log(resultRegister);
        }
        useEffect(() => {
            console.log(resultRegister);
        });

        function submit() {
            fetchData()
        }
        return (
          <>
          <MenuTow/>
            <div className="mb-5" style={{ marginTop: '134px' }}>
                <div style={{ color: '#747487' }} className="container form">
                    <div className="row">
                        <div style={{ color: '#fff' }} className="col-md-6 imglogin pt-5">
                            <div className="text-center pt-5">
                                <h1 className="login-title">ĐĂNG NHẬP</h1>
                                <h4 className="login-title mb-5">Chào mừng trở lại!</h4>
                                <h6 className="pt-5">Bạn mới sử dụng Home Martket? <a href="login.html">Đăng kí miễn phí</a>
                                </h6>
                            </div>
                        </div>
                        <div style={{ backgroundColor: "white" }} className="col-md-6 pt-5 pr-5 pl-5 ">

                            <div className="mb-3">
                                <label className="form-label">Email address</label>
                                <input type="email" className="form-control" id="exampleInputEmail1"
                                    onChange={event => setRegister({ ...register, userName: event.target.value })} />
                                <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1"
                                    onChange={event => setRegister({ ...register, passWord: event.target.value })} />
                            </div>

                            <button onClick={submit} type="submit" className="btn btn-primary w-100 mb-5">Submit</button>

                            <form className="signwith pb-4">
                                <div className="row">
                                    <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                                    <div className="col-md-4 text-center"><small>Hoặc đăng nhập bằng</small></div>
                                    <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                                </div>
                                <div className="d-grid gap-2 d-md-block text-center">
                                    <a className="btn fb" href="#" role="button">
                                        <div className="icon_fb icon2">
                                            <i className="fa fa-facebook" aria-hidden="true" />
                                        </div> <br />
                                        <h6>Facebook</h6>
                                    </a>
                                    <a className="btn gm" href="#" role="button">
                                        <div className="icongm icon2">
                                            <i className="fa fa-google" aria-hidden="true" />
                                        </div><br />
                                        <h6>Gmail</h6>
                                    </a>
                                </div>
                                <small className="pdt-md pt-4" id="agree-terms">
                                    <font style={{ verticalAlign: 'inherit' }}>
                                        <font style={{ verticalAlign: 'inherit' }}>
                                            Bằng cách đăng nhập, tôi đồng ý với </font>
                                    </font>
                                    <font style={{ verticalAlign: 'inherit' }}>
                                        <font style={{ verticalAlign: 'inherit' }}>Chính sách Bảo mật</font>
                                    </font>
                                    <font style={{ verticalAlign: 'inherit' }}>
                                        <font style={{ verticalAlign: 'inherit' }}> và </font>
                                    </font>
                                    <font style={{ verticalAlign: 'inherit' }}>
                                        <font style={{ verticalAlign: 'inherit' }}>Điều Khoản Dịch Vụ</font>
                                    </font>

                                    <font style={{ verticalAlign: 'inherit' }}>
                                        <font style={{ verticalAlign: 'inherit' }}> .</font>
                                    </font>
                                </small>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
    </>
        )
    }


    export default Register
