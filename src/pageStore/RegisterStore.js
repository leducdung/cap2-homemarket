import React from 'react';
import MenuTow from '../component/MenuTwo';
import Footer from '../component/Footer';

function RegisterStore(props) {
    return (
      <>
      <MenuTow/>
        <div style={{ marginTop: '120px' }} className="mb-5 pt-5">
            <div className="container">
                <div style={{ backgroundColor: 'white', boxShadow: '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)' }} className="row p-4">
                    <div className="col-md-5 imgEdit">

                    </div>
                    <div style={{}} className=" col-md-7 editUser w-100 p-5">
                        <h5 className='MenuRow'>ĐĂNG KÍ CHỦ CỬA HÀNG</h5>
                        <h6>Tên Cửa Hàng</h6>
                        <input type="text"
                            className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder="" />
                        <h6>Hình Ảnh</h6>
                        <input type="file"
                            className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder="" />
                        <h6>Email</h6>
                        <input type="email"
                            className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder="" />
                        <h6>Ngành Hàng Đăng Kí</h6>
                        <input type="text"
                            className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder="" />
                        <h6>Số Điện Thoại</h6>
                        <input type="text"
                            className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder="" />
                        <h6>Địa Chỉ</h6>
                        <input type="text"
                            className="form-control" name="" id="" aria-describedby="helpId" placeholder="" />
                        <button type="button" className="btnlogin p-2 mt-4">Đăng Kí</button>
                    </div>
                </div>
            </div>
        </div>
        <Footer></Footer>
    </>
    );
}

export default RegisterStore;